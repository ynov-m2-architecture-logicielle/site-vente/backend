import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import { AuthenticatedUser, Right } from "../entities/user/AuthenticatedUser.entity";

export const checkRight = (roles: Right[]) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    const id = res.locals.jwtPayload.id;
    const userRepository = getRepository(AuthenticatedUser);
    let user: AuthenticatedUser;
    try {
      user = await userRepository.findOneOrFail(id);
    } catch (id) {
      res.status(401).send("User doesn't exist");
    }

    // Check if array of authorized roles includes the user's role
    if (roles.indexOf(user.right) > -1) next();
    else res.status(401).send("Not authorized");
  };
};
