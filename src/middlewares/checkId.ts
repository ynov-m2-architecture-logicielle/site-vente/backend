import { NextFunction, Request, Response } from "express";
import { AuthenticatedUser } from "../entities/user/AuthenticatedUser.entity";
import { AuthenticationService } from "../services/auth.service";

export const checkId = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const authservice = new AuthenticationService();
  const user: AuthenticatedUser = await authservice.getUser(req.headers.authorization);

  if (user.id === req.params.id) next();
  else res.status(401).send("Not Authorized");
};
