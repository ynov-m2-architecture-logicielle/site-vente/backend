import { Stripe } from 'stripe'
import * as jwt from "jsonwebtoken"
import { Request, Response } from "express"
import { AuthenticationService } from '../services/auth.service'
import { AuthenticatedUser } from '../entities/user/AuthenticatedUser.entity'
import { Product } from '../models/product'

const URL = 'http://localhost:3000'
const stripe = new Stripe('sk_test_51ICMVxHmOoJeDnTyXbYv3PlQ3sw9xdETaQKTg5XrP79GEYe9vgVmfJYImbVBsACA8WaeVj6YH6340aR5u87R3BBK00VjzPX2E3', null)

class ShopController {

  public static createCheckoutSession = async (req: Request, res: Response) => {
    const products: Product[] = req.body.products
    const lineItems = []
    let productsAsString: string = ""

    products.forEach(p => {
      if (productsAsString == "") {
        productsAsString += p.id
      } else {
        productsAsString += ";" + p.id
      }
      lineItems.push({
        price_data: {
          currency: 'eur',
          product_data: {
            name: p.name,
            description: p.description,
            images: [p.imageSrc],
          },
          unit_amount: 0 + p.price.toString().replace(".", ""),
        },
        quantity: p.quantity,
      })
    })
    const session = await stripe.checkout.sessions.create({
      payment_method_types: ['card'],
      line_items: lineItems,
      mode: 'payment',
      success_url: `${URL}/shop/success/?token=${req.headers.authorization}&products=${productsAsString}`,
      cancel_url: `${URL}/shop/cancel`,
    })
    console.log("stripe session:", session.id)
    res.json({ id: session.id })
  }

  public static onSuccess = async (req: Request, res: Response) => {
    res.redirect(`http://localhost:4200/shop/success`)
  }

  public static onCancel = async (req, res) => {
    res.redirect('http://localhost:4200/shop/cancel')
  }
}

export default ShopController
