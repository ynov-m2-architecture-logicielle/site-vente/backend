import { validate } from "class-validator"
import { Request, Response } from "express"
import * as jwt from "jsonwebtoken"
import { getRepository } from "typeorm"
import config from "../config/config"
import { AuthenticatedUser } from "../entities/user/AuthenticatedUser.entity"
import { AuthenticationService } from "../services/auth.service"

class AuthController {

  static login = async (req: Request, res: Response) => {
    console.log(req.body)
    const email = req.body.email
    const password = AuthenticationService.decryptData(req.body.password)
    if (!(email && password)) {
      console.log("email or password missing")
      res.status(400).send()
    }

    const userRepository = getRepository(AuthenticatedUser)
    let user: AuthenticatedUser
    try {
      user = await userRepository.findOneOrFail({ where: { email } })
    } catch (error) {
      console.log(error)
      res.status(401).send()
      return
    }

    if (!user.checkIfUnencryptedPasswordIsValid(password)) {
      console.log("password not valid")
      res.status(401).send()
      return
    }

    const frontendUser = await AuthenticationService.getFrontendUser(user.id)
    res.status(200).send(frontendUser)
  }

  static register = async (req: Request, res: Response) => {
    console.log(req.body)
    const user = new AuthenticatedUser()
    user.email = req.body.email
    user.username = req.body.username
    user.password = AuthenticationService.decryptData(req.body.password)
    user.hashPassword()

    let errors = await validate(user)
    if (errors.length > 0) {
      res.status(400).send(errors)
      return
    }

    const userRepository = getRepository(AuthenticatedUser)

    const usersWithSameEmail = await userRepository.find({
      where: { email: user.email },
    })
    if (usersWithSameEmail.length > 0) {
      res.status(409).send("email already in use")
      return
    }

    let savedUser
    await userRepository.save(user).then((saved) => {
      savedUser = saved
    }).catch((error) => {
      console.log("error", error)
      res.status(500).send("internal error")
    })

    const frontendUser = await AuthenticationService.getFrontendUser(savedUser.id)
    res.status(201).send(frontendUser)
  }

  static changePassword = async (req: Request, res: Response) => {
    const id = res.locals.jwtPayload.userId

    const { oldPassword, newPassword } = req.body
    if (!(oldPassword && newPassword)) {
      res.status(400).send()
    }

    const userRepository = getRepository(AuthenticatedUser)
    let user: AuthenticatedUser
    try {
      user = await userRepository.findOneOrFail(id)
    } catch (id) {
      res.status(401).send()
    }

    if (!user.checkIfUnencryptedPasswordIsValid(oldPassword)) {
      res.status(401).send()
      return
    }

    user.password = newPassword
    const errors = await validate(user)
    if (errors.length > 0) {
      res.status(400).send(errors)
      return
    }

    user.hashPassword()
    userRepository.save(user)

    res.status(204).send()
  }

  static checkToken = async (req: Request, res: Response) => {

    let token: string = req.headers.authorization
    let jwtPayload
    try {
      jwtPayload = jwt.verify(token, config.jwtSecret)
      res.locals.jwtPayload = jwtPayload
    } catch (error) {
      console.log("token error:", error)
      res.status(401).send("Not valid token")
      return
    }

    const inOneWeek = new Date()
    inOneWeek.setDate(inOneWeek.getDate() + 7)
    const inOneWeekTime = inOneWeek.getTime() / 1000
    if (inOneWeekTime > jwtPayload.exp) {
      token = AuthenticationService.getNewToken(jwtPayload.id)
    }

    res.status(200).send({ token })
  }

  static checkAdminRight = async (req: Request, res: Response) => {
    res.status(200).send()
  }

}
export default AuthController
