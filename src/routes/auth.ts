import { Router } from "express"
import AuthController from "../controllers/AuthController"

const router = Router()

// Login route
router.post("/login", AuthController.login)

// Register
router.post("/register", AuthController.register)

export default router
