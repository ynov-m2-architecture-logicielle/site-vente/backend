import { Router } from "express"
import ShopController from "../controllers/ShopController";

const router = Router()

router.post("/create-checkout-session", ShopController.createCheckoutSession);

router.get("/success", ShopController.onSuccess);

router.get("/cancel", ShopController.onCancel);

export default router
