import * as bcrypt from "bcryptjs"
import { IsEmail, IsNotEmpty } from "class-validator"
import { Column, Entity, PrimaryGeneratedColumn, Unique } from "typeorm"
import { createHmac } from "crypto"
import config from "../../config/config"

export enum Right {
    DEFAULT = "default",
    ADMIN = "admin",
}

@Entity()
@Unique(["email"])
export class AuthenticatedUser {

    @PrimaryGeneratedColumn("uuid")
    id: string

    @Column()
    @IsNotEmpty()
    @IsEmail()
    email: string

    @Column()
    @IsNotEmpty()
    username: string

    @Column()
    @IsNotEmpty()
    password: string

    @Column()
    @IsNotEmpty()
    right: Right

    constructor(user: Partial<AuthenticatedUser> = {}) {
        this.id = user.id
        this.username = user.username
        this.right = user.right ? user.right : Right.DEFAULT
    }


    public hashPassword() {
        this.password = bcrypt.hashSync(this.getBase64HmacFromPassword(this.password))
    }

    private getBase64HmacFromPassword(password: string) {
        return createHmac('sha256', password + config.pepper).digest('base64')
    }

    checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
        return bcrypt.compareSync(this.getBase64HmacFromPassword(unencryptedPassword), this.password)
    }

}
