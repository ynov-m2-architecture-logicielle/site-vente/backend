import * as CryptoJS from "crypto-js"
import * as jwt from "jsonwebtoken"
import { getRepository } from "typeorm"
import config from "../config/config"
import { AuthenticatedUser } from "../entities/user/AuthenticatedUser.entity"

export class AuthenticationService {
  public getUser = async (token: string): Promise<AuthenticatedUser> => {
    let jwtPayload
    try {
      jwtPayload = jwt.verify(token, config.jwtSecret)
    } catch (error) {
      return null
    }
    const { userId } = jwtPayload
    const userRepository = getRepository(AuthenticatedUser)
    return await userRepository.findOne(userId, { select: ["id", "username"] })
  }

  public static getFrontendUser = async (userId: string) => {
    const userRepository = getRepository(AuthenticatedUser)
    const user: Partial<AuthenticatedUser> = await userRepository.findOne(userId, { select: ["id", "username", "right"] })
    const token = AuthenticationService.getNewToken(user.id)
    return { ...user, token }
  }

  public static getNewToken = (userId: string) => {
    return jwt.sign(
      { id: userId },
      config.jwtSecret,
      { expiresIn: "28d" },
    )
  }

  public static decryptData = (data: string): string => {
    const bytes = CryptoJS.AES.decrypt(data, config.CRYPTO_SECRET_KEY)
    if (bytes.toString()) {
      return JSON.parse(bytes.toString(CryptoJS.enc.Utf8))
    }
    return data
  }
}
